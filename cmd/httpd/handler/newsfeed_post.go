package handler

import (
	"net/http"
	"test-gin/platform/newsfeed"

	"github.com/gin-gonic/gin"
)

// Request body
type newsfeedPostRequest struct {
	Title string `json:"title"`
	Post  string `json:"post"`
}

// func NewsfeedPost(feed *newsfeed.Repo) gin.HandlerFunc {
func NewsfeedPost(feed newsfeed.Adder) gin.HandlerFunc {
	return func(c *gin.Context) {
		requestBody := newsfeedPostRequest{}
		c.Bind(&requestBody)

		item := newsfeed.Item{
			Title: requestBody.Title,
			Post:  requestBody.Post,
		}
		feed.Add(item)

		// c.JSON(http.StatusOK, map[string]string{
		// 	"hello": "Found",
		// })
		c.Status(http.StatusNoContent)
	}
}
